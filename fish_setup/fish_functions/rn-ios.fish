function rn-ios
  set_color black
  set_color -b blue
  echo "Building iOS App 🍎 on connected device..."
  set_color normal
  echo \n
  npx react-native run-ios --device --verbose
end

