function gl
  set_color yellow
  echo "GIT LOG"
  read -p "echo 'Simple (s) / Full (f): '" -l answer
  if test "$answer" = "f"
    git log 
  else
    git log --oneline
  end
end
