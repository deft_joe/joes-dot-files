#! /bin/bash

# Adapted from:
# http://ryan.himmelwright.net/post/scripting-tmux-workspaces/
# https://www.peterdebelak.com/blog/tmux-scripting/
# Session Name
SESSION="DEV-DEFAULT"
echo $SESSION

# Start New Session with variable name
tmux new -d -s $SESSION

# Name initial default window
tmux rename-window -t 1 'VIM_1'
#tmux send-keys -t $SESSION 'vim' C-m
tmux split-window -h -t $SESSION 
tmux split-window -v -p 10 -t $SESSION
tmux send-keys -t $SESSION 'cava' C-m
tmux new-window -t $SESSION:2 -n VIM_2
#tmux send-keys -t $SESSION 'vim' C-m
#tmux split-window -h -p 10 -t $SESSION
tmux split-window -h -t $SESSION 
tmux new-window -t $SESSION:3 -n SHELL_1 
tmux new-window -t $SESSION:4 -n SHELL_2 
#tmux send-keys -t $SESSION:4 '~/ClickUp/clickup.AppImage'
tmux select-window -t 'VIM_1'
tmux attach-session -t $SESSION


