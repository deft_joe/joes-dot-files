"module.exports = {" http://vimdoc.sourceforge.net/htmldoc/intro.html#key-notation
" Modified: https://github.com/ziegfiroyt/react-vimrc/blob/master/.vimrc
" Install vim-plug if not installed
set shell=/usr/local/bin/fish
set noerrorbells 
"set visualbell
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall
endif
"
" Remap <Esc> to 'jk'
" inoremap jk <Esc>
imap jk <Esc>
set timeoutlen=1000
"
" Reduce <Esc> Lag
set timeout timeoutlen=500 ttimeoutlen=10
"terminal colors
set termguicolors
let $NVIM_TUI_ENABLE_TRUE_COLOR=1
" natural splits
set splitbelow
set splitright
" Tabs #tabs
" - Two spaces wide
set tabstop=3
set softtabstop=3
" - Expand them all
set expandtab
" - Indent by 2 spaces by default
set shiftwidth=2
" turn off mouse
set mouse=""
""" Leader #leader
let g:mapleader = ','
let g:maplocalleader = "\<Space>"
"nnoremap <silent> <leader>      :<c-u>WhichKey ','<CR>
"nnoremap <silent> <localleader> :<c-u>WhichKey  '<Space>'<CR>
"" Format Options #format-options
set formatoptions=tcrq
set textwidth=80
set guicursor=a:blinkon100
set guicursor=n-v-c-sm:block,i-ci-ve:ver25,r-cr-o:hor20
" See Filename
set laststatus=2
" Follow current directory for opening buffers
set autochdir
"" Libraries
call plug#begin()
Plug 'neoclide/coc.nvim', {'branch': 'release'}
"Plug 'neoclide/coc.nvim', {'branch': 'master', 'do': 'yarn install --frozen-lockfile'}
Plug 'jeffkreeftmeijer/vim-numbertoggle'
Plug 'vim-scripts/auto-pairs-gentle'
" Following two pkgs for jsx & tsx support
Plug 'HerringtonDarkholme/yats.vim'
Plug 'maxmellon/vim-jsx-pretty'
Plug 'scrooloose/nerdcommenter'
Plug 'matze/vim-move'
Plug 'lilydjwg/colorizer'
Plug 'tmux-plugins/vim-tmux-focus-events'
Plug 'roxma/vim-tmux-clipboard'
Plug 'francoiscabrol/ranger.vim'
Plug 'rbgrouleff/bclose.vim'
Plug 'prettier/vim-prettier', { 'do': 'yarn install' }"
Plug 'vimwiki/vimwiki'
Plug 'guns/xterm-color-table.vim'
Plug 'jparise/vim-graphql'
Plug 'machakann/vim-highlightedyank'
Plug 'machakann/vim-sandwich'
Plug 'farmergreg/vim-lastplace'
Plug 'bluz71/vim-nightfly-guicolors'
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'nvim-telescope/telescope-fzy-native.nvim'
Plug 'qpkorr/vim-bufkill'
Plug 'duff/vim-scratch'
Plug 'itchyny/lightline.vim'
call plug#end()
" Sourcing
source $HOME/dotfiles/nvim-configs/telescope.vim

nnoremap <leader>ff <cmd>Telescope live_grep<cr>
nnoremap <leader>fg <cmd>Telescope find_files<cr>
nnoremap S <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>

"set statusline^=%{coc#status()}
"set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

"" enable 24bit true color
if (has("termguicolors"))
 set termguicolors
endif


"" enable the theme
syntax enable
colorscheme nightfly 
let g:nightflyCursorColor = 1
let g:nightflyUnderlineMatchParen = 1
set background=dark
let g:lightline = { 
\ 'colorscheme': 'nightfly',
 \ 'mode_map': {
        \ 'n' : 'N',
        \ 'i' : 'I',
        \ 'R' : 'R',
        \ 'v' : 'V',
        \ 'V' : 'VL',
        \ "\<C-v>": 'VB',
        \ 'c' : 'C',
        \ 's' : 'S',
        \ 'S' : 'SL',
        \ "\<C-s>": 'SB',
        \ 't': 'T',
        \ },
      \ }


""" Customize colors
hi Pmenu guibg='navy'
func! s:my_colors_setup() abort
    " this is an example
    hi Pmenu guibg=#d7e5dc gui=NONE
    hi PmenuSel guibg=#b7c7b7 gui=NONE
    hi PmenuSbar guibg=#bcbcbc
    hi PmenuThumb guibg=#585858
endfunc

augroup colorscheme_coc_setup | au!
    au ColorScheme * call s:my_colors_setup()
augroup END


" Vim Wiki
let g:vimwiki_list = [{'path': '~/NOTES/',
                      \ 'syntax': 'markdown', 'ext': '.md'}]

" Vim Ranger
let g:ranger_replace_netrw = 1
let g:ranger_command_override = 'ranger --cmd "set show_hidden=true"'
" Open VimRanger when loading Vim
" https://stackoverflow.com/questions/6821033/vim-how-to-run-a-command-immediately-when-starting-vim
" https://github.com/francoiscabrol/ranger.vim
autocmd VimEnter * RangerCurrentDirectory

"Vim-Move
" Use <Ctrl-h,j,k,l> to move text
let g:move_key_modifier = 'C'

"Auto-pairs
let g:AutoPairsUseInsertedCount = 1

" Vim-JSX-Pretty
let g:vim_jsx_pretty_highlight_close_tag = 1
let g:vim_jsx_pretty_colorful_config = 1 " default 0

" NerdCommenter
" toggle with <leader>c<space>
let g:NERDCommentEmptyLines = 1
let g:NERDTrimTrailingWhitespace = 1
let g:NERDToggleCheckAllLines = 1

" CocSettings
" Basic Settings: https://github.com/neoclide/coc.nvim#example-vim-configuration
set hidden
set nobackup
set nowritebackup
set cmdheight=2
set updatetime=300
set shortmess+=c
set signcolumn=yes

" NOTE: For CocIntellisense: Install language-specific extensions:
" https://github.com/neoclide/coc.nvim/wiki/Using-coc-extensions#implemented-coc-extensions
" e.g., run: CocInstall coc-tsserver

" Use Tab for Autocomplete
" use <tab> for trigger completion and navigate to the next complete item
function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~ '\s'
endfunction
inoremap <silent><expr> <Tab>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<Tab>" :
      \ coc#refresh()
inoremap <silent><expr> <c-space> coc#refresh()
" https://github.com/prettier/vim-prettier
" Coc-Pretter -- For save on format, ensure CocConfig is has the following entry
" (JSON format):
"{
  ""coc.preferences.formatOnSaveFiletypes": [
    ""javascript",
    ""javascriptreact",
    ""typescript",
    ""typescriptreact",
    ""json",
    ""graphql",
    ""css",
    ""Markdown"
  "]
"}

" Vim-Workspace
"if empty(glob('~/.vim/autoload/plug.vim'))
  "silent execute "!curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
  "autocmd VimEnter * PlugInstall | source $MYVIMRC
"endif

" Vim-Numbertoggle
:set number relativenumber

:augroup numbertoggle
:  autocmd!
:  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
:  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
:augroup END

"Crosshairs
set cursorline! cursorcolumn!
hi CursorLine   cterm=NONE ctermbg=234 ctermfg=NONE guibg=#1c1c1c guifg=NONE
hi CursorColumn cterm=NONE ctermbg=234 ctermfg=NONE guibg=#1c1c1c guifg=NONE
nnoremap <Leader>c :set cursorline! cursorcolumn!<CR>

"augroup CursorColumn cterm=NONE ctermbg=234 guibg=235 guifg=NONE
  "au!
  "au VimEnter,WinEnter,BufWinEnter * setlocal cursorcolumn
  "au WinLeave * setlocal nocursorcolumn
"augroup END

"augroup CursorLine
  "au!
  "au VimEnter,WinEnter,BufWinEnter * setlocal cursorline
  "au WinLeave * setlocal nocursorline
"augroup END

" Comments Color
hi Comment guifg=#7E7F87
hi Pmenu ctermbg=black ctermfg=white

" Comments for JSONC
autocmd FileType json syntax match Comment +\/\/.\+$+

" Disable swap files
" Notify if file has been edited in another vim session
" Code from here:
" https://www.reddit.com/r/vim/comments/5u3hgj/open_a_file_which_is_already_opened_by_vim_in/ddru0yv?utm_source=share&utm_medium=web2x

set noswapfile         " Write swap and backup files

set noautoread         " together with :checktime (and set confirm), prompt to reload file
set confirm            " get a dialog when :q, :w, or :wq fails"

augroup AutoSwap
      autocmd!
          autocmd SwapExists *  call AS_HandleSwapfile(expand('<afile:p'), v:swapname)
        augroup END
        function! AS_HandleSwapfile (filename, swapname)
           " if swapfile is older than file itself, just get rid of it
          if getftime(v:swapname) < getftime(a:filename)
            call delete(v:swapname)
            let v:swapchoice = 'e'
          endif
        endfunction
autocmd BufWritePost,BufReadPost,BufLeave *
            \ if isdirectory(expand("<amatch:h")) | let &swapfile = &modified | endif
augroup checktime
      autocmd!
          if !has("gui_running")
            autocmd BufEnter,CursorMoved,CursorMovedI * checktime
            autocmd FocusGained,BufEnter,FocusLost,WinLeave * checktime
          endif
        augroup END
"
function! Scratch()
    split
    noswapfile hide enew
    setlocal buftype=nofile
    setlocal bufhidden=hide
    "setlocal nobuflisted
    "lcd ~
    file scratch
endfunction

nnoremap <Leader><Space>  <cmd>:call Scratch() <CR>
xmap <leader>a  <Plug>(coc-react-refactor)
nmap <leader>a  <Plug>(coc-react-refactor)
